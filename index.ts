
function twoSum(nums: number[], target: number): number[] {
    // создаем таблицу
    const hashmap: Record<string, number> = {}

    for (let i = 0; i < nums.length; i++) {
        // Делаем hashMap из ключей - значений массива, и значений - индексов массива
        hashmap[nums[i]] = i
    }
    // console.log('hash', hashmap)
    for (let i = 0; i < nums.length; i++) {
        // Находим второе число суммы
        const complement = target - nums[i];

        // проверяем, есть ли  второе число в таблице
        if(hashmap[complement] && hashmap[complement] !== i){
            return [i,  hashmap[complement]]
        }
        hashmap[nums[i]] = i
    }

    return []
}






console.log('twoSum', twoSum([2,7,11,15], 9))
console.log('twoSum1', twoSum([3,2,4],  6))
console.log('twoSum2', twoSum([-3,-3],  -6))