function twoSum(nums, target) {
    // создаем таблицу
    const hashmap = new Map()

    // console.log('hash', hashmap)
    for (let i = 0; i < nums.length; i++) {
        // Находим второе число суммы
        const y = target - nums[i];

        // проверяем, если complement в таблице
        if(hashmap.has(y)){
            return [i, hashmap.get(y)]
        }

        hashmap.set(nums[i], i)
    }

    return []
}


console.log('twoSum', twoSum([2,7,11,15], 9))
console.log('twoSum1', twoSum([3,2,4],  6))
console.log('twoSum2', twoSum([-3,-3],  -6))